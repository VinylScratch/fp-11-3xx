	data Term = Var String 
          | Lam String Term 
          | App Term Term
          deriving(Eq) 

instance Show Term where
    show (Var x) = x
    show (App t1 t2) = "(" ++ (show t1) ++ " " ++ (show t2)++ ")"
    show (Lam v t) = "(\\" ++ v ++ "." ++ (show t) ++ ")"  

getExlusiveVar :: [String] -> String -> Int -> String
getExlusiveVar arr x n | elem (x ++ (show n)) arr = getExlusiveVar arr x (n+1)
                       | otherwise = x ++ (show n)  

changeAllVars :: [String] -> Term -> Term
changeAllVars arr (Lam x term) | elem x arr = Lam x (changeAllVars (filter (\el -> el /= x) arr) term)
                                        | otherwise = Lam x (changeAllVars arr term)
changeAllVars arr (App t1 t2) = App (changeAllVars arr t1) (changeAllVars arr t2)
changeAllVars arr (Var x) | elem x arr = Var (getExlusiveVar arr x 0)
                                     | otherwise = Var x 

--subst e x v == e[v/x] -- v must be closed (no free Vars) --
subst :: [String] -> Term -> String -> Term -> Term
subst arr (Var y) x v = if x == y then (changeAllVars arr v) else (Var y)
subst arr (Lam y e) x v = if x == y then (Lam y e) else (Lam y (subst (y:arr) e x v))
subst arr (App e1 e2) x v = App (subst arr e1 x v) (subst arr e2 x v) 

--Eval in one step
eval1' :: Term -> Term
eval1' (App (Var v) t) = App (Var v) (eval1' t)
eval1' (App (Lam x e) v) = subst [] e x (eval1' v)
eval1' (App (App t1 t2) t3) = App (eval1' (App t1 t2)) t3
eval1' t = t

--Eval in more step
eval :: Term -> Term
eval (App (Var v) t) = App (Var v) (eval t)
eval (App (Lam x e) v) = eval $ subst [] e x (eval v)
eval (App (App t1 t2) t3) = eval $ App (eval (App t1 t2)) t3
eval t = t 

y = Var "y"
x = Var "x"
id' = Lam "x" x
foo = App (Lam "y" y) y

--((\x.\y.x) z) ((\x. (x x))(\x. (x x))) == z
ex1 = App (App (Lam "x" (Lam "y" (Var "x"))) (Var "z")) (App (Lam "x" (App (Var "x") (Var "x"))) (Lam "x" (App (Var "x") (Var "x"))))
ex2 = App (Lam "x" (Lam "y" (Var "y"))) (Var "y") --(\x.\y y) y
ex3 = App (Lam "x" (Lam "y" (Var "x"))) (Lam "z" (Var "z")) ----(\x.\y x) (\z.z)
ex4 = App (Lam "x" (Lam "y" (Var "x"))) (Var "y") --(\x.\y x) y
ex5 = App (Lam "x" (Lam "y" (Var "y"))) (Var "z") --(\x.\y y) z

ex6 = App id' (App id' (Lam "z" (App id' (Var "z"))))
ex7 = App (Lam "x" id') (Var "x")
ex8 = App (App (Lam "x" (Lam "y" (App (Var "x") (Var "y")))) (Lam "z" (Var "z"))) (Lam "u" (Lam "v" (Var "v")))
ex9 = App (Lam "x" $ Lam "y" $ Var "x") (Lam "x" $ Var "y")
ex10 = App (Lam "x" $ Lam "y" $ Var "x") (Var "z")
ex11 = App (App (App (App (Lam "m" $ Lam "n" $ Lam "s" $ Lam "z" $ App (App (Var "m") (App (Var "n") (Var "s"))) (Var "z")) (Lam "s" $ Lam "z" $ App (Var "s") $ App (Var "s") $ Var "z")) (Lam "s" $ Lam "z" $ App (Var "s") $ App (Var "s") $ Var "z")) (Var "S")) (Var "Z")